const express = require("express");
const router = express.Router();
const Category = require("../models/Category");
const mongoose = require("mongoose");

router.get("/", async (req, res) => {
  const categoryList = await Category.find();
  console.log("categoryList: ", categoryList);

  if (!categoryList) {
    res.status(500).json({ success: false });
  }
  res.status(200).send(categoryList);
});

router.get("/:id", async (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    res
      .status(400)
      .json({ success: false, message: "Id not a valid ObjectId" });
  } else {
    const category = await Category.findById(req.params.id);
    if (!category && !category?.id) {
      return res
        .status(400)
        .json({
          success: false,
          message: "The category with the given ID not exists",
        });
    }
    res.status(200).send(category);
  }
});

router.post("/", async (req, res) => {
  if (!req.body.name || !req.body.name.length) {
    return res.status(422).send({
      message: "name is required and must not be empty",
    });
  }
  let category = new Category({
    name: req.body.name,
    icon: req.body.icon,
    color: req.body.color,
  });

  category = await category.save();

  if (!category) return res.status(404).send("Category cannot be created");
  res.send(category);
});

router.put("/:id", async (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    res
      .status(400)
      .json({ success: false, message: "Id not a valid ObjectId" });
  } else {
    const category = await Category.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        icon: req.body.icon,
        color: req.body.color,
      },
      {
        new: true,
      }
    );

    if (!category) return res.status(404).send("Category cannot be created");
    res.send(category);
  }
});

router.delete("/:id", (req, res) => {
  Category.findByIdAndRemove(req.params.id)
    .then((category) => {
      if (category) {
        return res
          .status(200)
          .json({ success: true, message: "Category deleted successfully" });
      } else {
        return res
          .status(404)
          .json({ success: false, message: "Category cannot find" });
      }
    })
    .catch((err) => {
      return res.status(400).json({ success: false, error: err });
    });
});

module.exports = router;
