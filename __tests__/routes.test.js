const request = require("supertest");
const path = require("path");
const app = require("../app");
const User = require("../models/User");
const Category = require("../models/Category");
const Product = require("../models/Product");

describe("Login", () => {
  beforeAll(async () => {
    await User.deleteMany({}); // clear DB before all tests

    const user = new User({
      email: "test@example.com",
      password: "123456",
      displayName: "Test",
    });

    await user.save();
  });

  afterAll(async () => {
    await User.deleteMany({}); // clear DB after all tests
  });

  describe("Post /login", () => {
    it("should return error: email is required", async (done) => {
      const res = await request(app).post("/login").send({
        password: "123456",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /login", () => {
    it("should return error: email must not be empty", async (done) => {
      const res = await request(app).post("/login").send({
        email: "",
        password: "123456",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /login", () => {
    it("should return error: password is required", async (done) => {
      const res = await request(app).post("/login").send({
        email: "test@example.com",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /login", () => {
    it("should return error: password must not be empty", async (done) => {
      const res = await request(app).post("/login").send({
        email: "test@example.com",
        password: "",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /login", () => {
    it("should return user", async (done) => {
      const res = await request(app).post("/login").send({
        email: "test@example.com",
        password: "123456",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty("email", "test@example.com");
      done();
    });
  });

  describe("Post /login", () => {
    it("should return error: User not found", async (done) => {
      const res = await request(app).post("/login").send({
        email: "test1@example.com",
        password: "123456",
      });
      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty("message");
      expect(res.body["message"]).toContain("User not found");
      done();
    });
  });

  describe("Post /login", () => {
    it("should return error: Password is incorrect", async (done) => {
      const res = await request(app).post("/login").send({
        email: "test@example.com",
        password: "1234533",
      });
      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty("message");
      expect(res.body["message"]).toContain("Password is incorrect");
      done();
    });
  });
});

describe("Register", () => {
  beforeEach(async () => {
    await User.deleteMany({}); // clear DB before each test
  });

  afterEach(async () => {
    await User.deleteMany({}); // clear DB after each test
  });

  describe("Post /register", () => {
    it("should return error: email is required", async (done) => {
      const res = await request(app).post("/register").send({
        password: "123456",
        displayName: "Test2",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: email must not be empty", async (done) => {
      const res = await request(app).post("/register").send({
        email: "",
        password: "123456",
        displayName: "Test2",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: password is required", async (done) => {
      const res = await request(app).post("/register").send({
        email: "test@example.com",
        displayName: "Test2",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: password must not be empty", async (done) => {
      const res = await request(app).post("/register").send({
        email: "test@example.com",
        password: "",
        displayName: "Test2",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: displayName is required", async (done) => {
      const res = await request(app).post("/register").send({
        email: "test@example.com",
        password: "123456",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: displayName must not be empty", async (done) => {
      const res = await request(app).post("/register").send({
        email: "test@example.com",
        password: "123456",
        displayName: "",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post /register", () => {
    it("should create an user", async (done) => {
      const res = await request(app).post("/register").send({
        email: "test@example.com",
        password: "123456",
        displayName: "Test",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty("message", "success");
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: duplicate record", async (done) => {
      const resCreate = await request(app).post("/register").send({
        email: "test@example.com",
        password: "123456",
        displayName: "Test",
      });
      expect(resCreate.statusCode).toEqual(200);
      expect(resCreate.body).toHaveProperty("message", "success");

      const res = await request(app).post("/register").send({
        email: "test@example.com",
        password: "123456",
        displayName: "Test",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty("error");

      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: Email must be at least 8 characters long", async (done) => {
      const res = await request(app).post("/register").send({
        email: "b@a.com",
        password: "123456",
        displayName: "Test",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty("error");
      expect(res.body["error"]).toContain(
        "Email must be at least 8 characters long"
      );
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: Email already exists", async (done) => {
      const resCreate = await request(app).post("/register").send({
        email: "test@example.com",
        password: "123456",
        displayName: "Test",
      });
      expect(resCreate.statusCode).toEqual(200);
      expect(resCreate.body).toHaveProperty("message", "success");

      const res = await request(app).post("/register").send({
        email: "test@example.com",
        password: "333444",
        displayName: "Test2",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty("error");
      expect(res.body["error"]).toContain("Email already exists");
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: Password must be at least 6 characters long", async (done) => {
      const res = await request(app).post("/register").send({
        email: "test@example.com",
        password: "1234",
        displayName: "Test",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty("error");
      expect(res.body["error"]).toContain(
        "Password must be at least 6 characters long"
      );
      done();
    });
  });

  describe("Post /register", () => {
    it("should create user: valid email address", async (done) => {
      const res = await request(app).post("/register").send({
        email: "test.aqua123@doc.google.com",
        password: "123445",
        displayName: "Test345",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty("message", "success");
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: Email must be a valid email address", async (done) => {
      const res = await request(app).post("/register").send({
        email: "test;@doc.google.com",
        password: "123445",
        displayName: "Test345",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty("error");
      expect(res.body["error"]).toContain("is not a valid email address!");
      done();
    });
  });

  describe("Post /register", () => {
    it("should return error: Email must be a valid email address", async (done) => {
      const res = await request(app).post("/register").send({
        email: "testadoc.google.com",
        password: "123445",
        displayName: "Test345",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty("error");
      expect(res.body["error"]).toContain("is not a valid email address!");
      done();
    });
  });
});

describe("Categories", () => {
  //API test case
  beforeEach(async () => {
    await Category.deleteMany({}); // clear DB before each test
  });

  afterEach(async () => {
    await Category.deleteMany({}); // clear DB after each test
  });
  // Tạo Category thiếu name
  describe("Post/Categories", () => {
    it("should return error: name is required", async (done) => {
      const res = await request(app).post("/categories").send({
        icon: "Book",
        color: "Blue",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post/Categories", () => {
    it("should return error: name must not be empty", async (done) => {
      const res = await request(app).post("/categories").send({
        icon: "Book",
        color: "Blue",
      });
      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Post/Categories", () => {
    it("should create an category", async (done) => {
      const res = await request(app).post("/categories").send({
        name: "Sách",
        icon: "Book",
        color: "Blue",
      });
      expect(res.statusCode).toEqual(200);
      done();
    });
  });

  describe("Get/Categories/:Id", () => {
    it("should return category detail", async (done) => {
      const createRes = await request(app).post("/categories").send({
        name: "Bút chì",
        icon: "Pencil",
        color: "Blue",
      });
      expect(createRes.statusCode).toEqual(200);
      const categoryId = createRes.body._id;
      const res = await request(app).get(`/categories/${categoryId}`);
      expect(res.statusCode).toEqual(200);
      done();
    });
  });

  describe("Get/Categories/:Id", () => {
    it("should return error the category with the given ID not exists", async (done) => {
      const res = await request(app).get(
        "/categories/646ecc29f41a113d10c5b00b"
      );
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Get/Categories/:Id", () => {
    it("should return error the category with Id not a valid ObjectId", async (done) => {
      const res = await request(app).get("/categories/123456789");
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty("message");
      done();
    });
  });

  describe("Get/Categories", () => {
    it("Get category all", async (done) => {
      const res = await request(app).get("/categories");
      expect(res.statusCode).toEqual(200);
      done();
    });
  });

  describe("Update/Categories/:Id", () => {
    it("should return category update success", async (done) => {
      const createRes = await request(app).post("/categories").send({
        name: "Bàn ghê",
        icon: "Table",
        color: "Pink",
      });
      expect(createRes.statusCode).toEqual(200);
      const categoryId = createRes.body._id;
      const res = await request(app).put(`/categories/${categoryId}`).send({
        name: "Bàn ghế",
        icon: "Table desk",
        color: "Green",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toEqual({
        __v: 0,
        _id: categoryId,
        name: "Bàn ghế",
        icon: "Table desk",
        color: "Green",
      });
      done();
    });
  });

  describe("Delete/Categories/:Id", () => {
    it("should return error category cannot find ", async (done) => {
      const resDeleteError = await request(app).delete(
        `/categories/646ee31477addf015034fd10`
      );
      expect(resDeleteError.statusCode).toEqual(404);
      done();
    });
    it("should return delete category success ", async (done) => {
      const createRes = await request(app).post("/categories/").send({
        name: "Bàn ghê",
        icon: "Table",
        color: "Pink",
      });
      expect(createRes.statusCode).toEqual(200);
      const categoryId = createRes.body._id;
      const resDelete = await request(app).delete(`/categories/${categoryId}`);
      expect(resDelete.statusCode).toEqual(200);
      done();
    });
  });

  //data-driven test cases
  describe("Category  data-driven", () => {
    // Test data for creating categories
    const categoriesData = [
      {
        name: "Thước",
        icon: "Rulers",
        color: "Black",
      },
      {
        name: "Cặp",
        color: "Black",
      },
      {
        name: "Quần áo",
        icon: "Clothes",
        color: "Blue",
      },
      {
        name: "Cục tẩy",
      },
      // add more data
    ];

    describe("Post/Category", () => {
      categoriesData.forEach((category) => {
        describe(`should return create new categories`, () => {
          it(`create a category ${category.name}`, async () => {
            const createResponse = await request(app)
              .post("/categories")
              .send(category);

            expect(createResponse.statusCode).toEqual(200);
          });
        });
      });
    });

    describe("Get/Category", () => {
      categoriesData.forEach((category) => {
        describe(`get /categories/:id`, () => {
          it(`get a category ${category.name}`, async () => {
            const createResponse = await request(app)
              .post("/categories")
              .send(category);

            expect(createResponse.statusCode).toEqual(200);
            const cateId = createResponse.body._id;
            const getResponse = await request(app).get(`/categories/${cateId}`);

            expect(getResponse.statusCode).toEqual(200);
            expect(getResponse.body).toEqual({
              _id: cateId,
              __v: 0,
              ...category,
            });
          });
        });

        describe(`Get/category`, () => {
          it("Get category all", async (done) => {
            const res = await request(app).get("/categories");
            expect(res.statusCode).toEqual(200);
            done();
          });
        });
      });
    });

    describe("Delete/Category", () => {
      categoriesData.forEach((category) => {
        describe(`delete /categories/:id`, () => {
          it(`delete a category ${category.name}`, async (done) => {
            const createResponse = await request(app)
              .post("/categories")
              .send(category);

            expect(createResponse.statusCode).toEqual(200);
            const cateId = createResponse.body._id;
            const resDelete = await request(app).delete(
              `/categories/${cateId}`
            );
            expect(resDelete.statusCode).toEqual(200);
            done();
          });
        });
      });
    });

    describe("Update/Category", () => {
      categoriesData.forEach((category) => {
        describe(`update /categories/:id`, () => {
          it(`update a category ${category.name}`, async (done) => {
            const createResponse = await request(app)
              .post("/categories")
              .send(category);

            expect(createResponse.statusCode).toEqual(200);
            const cateId = createResponse.body._id;
            const res = await request(app).put(`/categories/${cateId}`).send({
              name: "Bàn ghế",
              icon: "Table desk",
              color: "Green",
            });
            expect(res.statusCode).toEqual(200);
            expect(res.body).toEqual({
              __v: 0,
              _id: cateId,
              name: "Bàn ghế",
              icon: "Table desk",
              color: "Green",
            });
            done();
          });
        });
      });
    });
  });
});

describe("Product", () => {
  beforeAll(async () => {
    await Product.deleteMany({}); // clear DB before all tests
  });

  afterAll(async () => {});
  describe("API product testcases", () => {
    describe("Post/Products", () => {
      it("should return error: No image in the request", async (done) => {
        const res = await request(app)
          .post("/products")
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", "646f23199cb8420df0be77c5")
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");
        expect(res.statusCode).toEqual(400);
        // expect(res.body).toHaveProperty('message')
        done();
      });

      it("should return error: Product Invalid Category", async (done) => {
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const res = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", "646f23199cb8420df0be77c5")
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");
        expect(res.statusCode).toEqual(400);
        done();
      });

      it("should return Product", async (done) => {
        const createResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createResponse.statusCode).toEqual(200);
        const catId = createResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const res = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(res.statusCode).toEqual(200);
        done();
      });
    });

    describe("Update/Product/:id", () => {
      it("should return error: Invalid category", async (done) => {
        const createCatResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createCatResponse.statusCode).toEqual(200);
        const catId = createCatResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const createProductResponse = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(createProductResponse.statusCode).toEqual(200);
        const productId = createProductResponse.body._id;
        const updateProductResponse = await request(app)
          .put(`/products/${productId}`)
          .send({
            category: "646f23199cb8420df0be77c5",
            name: "Abc",
            description: "xxx",
            richDescription: "yyy",
            image:
              "https://tdeskweb.qa.public.dev.tmtco.org/assets/images/wellcome.png",
          });
        expect(updateProductResponse.statusCode).toEqual(400);
        done();
      });

      it("should return error: Product cannot be updated", async (done) => {
        const createCatResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createCatResponse.statusCode).toEqual(200);
        const catId = createCatResponse.body._id;
        const updateProductResponse = await request(app)
          .put(`/products/64702e7c9a0d7f2c20aab783`)
          .send({
            category: catId,
            name: "Abc",
            description: "xxx",
            richDescription: "yyy",
            image:
              "https://tdeskweb.qa.public.dev.tmtco.org/assets/images/wellcome.png",
          });
        expect(updateProductResponse.statusCode).toEqual(500);
        done();
      });

      it("should return : update product success", async (done) => {
        const createCatResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createCatResponse.statusCode).toEqual(200);
        const catId = createCatResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const createProductResponse = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(createProductResponse.statusCode).toEqual(200);
        const productId = createProductResponse.body._id;
        const updateProductResponse = await request(app)
          .put(`/products/${productId}`)
          .send({
            category: catId,
            name: "Abc",
            description: "xxx",
            richDescription: "yyy",
            image:
              "https://tdeskweb.qa.public.dev.tmtco.org/assets/images/wellcome.png",
          });
        expect(updateProductResponse.statusCode).toEqual(200);
        done();
      });
    });

    describe("Get/products", () => {
      it("should return: products", async (done) => {
        const getAllProductResponse = await request(app).get(`/products`);
        expect(getAllProductResponse.statusCode).toEqual(200);
        done();
      });
      it("should return: products by categoryId", async (done) => {
        const createCatResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createCatResponse.statusCode).toEqual(200);
        const catId = createCatResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const createProductResponse = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(createProductResponse.statusCode).toEqual(200);
        const getAllProductResponse = await request(app).get(
          `/products?category=${catId}`
        );
        expect(getAllProductResponse.statusCode).toEqual(200);
        done();
      });

      it("should return error: categoryId invalid", async (done) => {
        const createCatResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createCatResponse.statusCode).toEqual(200);
        const catId = createCatResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const createProductResponse = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(createProductResponse.statusCode).toEqual(200);
        const getAllProductResponse = await request(app).get(
          `/products?category=64702eb19a0d7f2c20aab784`
        );
        expect(getAllProductResponse.statusCode).toEqual(200);
        done();
      });
    });

    describe("Get/products/:Id", () => {
      it("should return: product", async (done) => {
        const createCatResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createCatResponse.statusCode).toEqual(200);
        const catId = createCatResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const createProductResponse = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(createProductResponse.statusCode).toEqual(200);
        const productId = createProductResponse.body._id;

        const getProductResponse = await request(app).get(
          `/products/${productId}`
        );
        expect(getProductResponse.statusCode).toEqual(200);
        done();
      });

      it("should return error:The product with the given ID not exists", async (done) => {
        const getProductResponse = await request(app).get(
          `/products/64702e7c9a0d7f2c20aab783`
        );

        expect(getProductResponse.statusCode).toBe(500);
        done();
      });
    });

    describe("Get/products/count", () => {
      it("should return:count product", async (done) => {
        const createCatResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createCatResponse.statusCode).toEqual(200);
        const catId = createCatResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const createProductResponse = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(createProductResponse.statusCode).toEqual(200);
        const getProductResponse = await request(app).get(
          `/products/get/count`
        );

        expect(getProductResponse.statusCode).toBe(200);
        done();
      });
    });

    describe("/get/featured/:count", () => {
      it("should return:count featured empty", async (done) => {
        const createCatResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createCatResponse.statusCode).toEqual(200);
        const catId = createCatResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const createProductResponse = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(createProductResponse.statusCode).toEqual(200);
        const getProductResponse = await request(app).get(
          `/products/get/featured`
        );

        expect(getProductResponse.statusCode).toBe(404);
        done();
      });

      it("should return:count featured count =5", async (done) => {
        const createCatResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createCatResponse.statusCode).toEqual(200);
        const catId = createCatResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const createProductResponse = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(createProductResponse.statusCode).toEqual(200);
        const getProductResponse = await request(app).get(
          `/products/get/featured/5`
        );

        expect(getProductResponse.statusCode).toBe(200);
        done();
      });
    });

    describe("/delete/:id", () => {
      it("should return error:productId is empty", async (done) => {
        const getProductResponse = await request(app).delete(`/products`);

        expect(getProductResponse.statusCode).toBe(404);
        done();
      });
      it("should return error:productId = 1234567", async (done) => {
        const getProductResponse = await request(app).delete(
          `/products/1234567`
        );

        expect(getProductResponse.statusCode).toBe(400);
        done();
      });

      it("should return error:productId not exsit", async (done) => {
        const getProductResponse = await request(app).delete(
          `/products/22222fae4d4f602e7cd2b840`
        );

        expect(getProductResponse.statusCode).toBe(404);
        done();
      });

      it("should return delete product success", async (done) => {
        const createResponse = await request(app).post("/categories").send({
          name: "Quần áo",
          icon: "Clothes",
          color: "Blue",
        });

        expect(createResponse.statusCode).toEqual(200);
        const catId = createResponse.body._id;
        const imagePath = path.join(__dirname, "assets", "depzai.jpeg");
        const res = await request(app)
          .post("/products")
          .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
          .field("name", "Bút chì 2B")
          .field("description", "Bút chì rất đẹp sản xuất ở vietnam")
          .field("richDescription", "Quá tuyệt vời")
          .field("brand", "Dụng cụ học tập")
          .field("price", "7000")
          .field("category", `${catId}`)
          .field("countInStock", "10")
          .field("rating", "4")
          .field("numReviews", "6")
          .field("isFeatured", "true");

        expect(res.statusCode).toEqual(200);
        const productId = res.body._id;
        const getProductResponse = await request(app).delete(
          `/products/${productId}`
        );

        expect(getProductResponse.statusCode).toBe(200);
        done();
      });
    });
  });

  describe("data-driven product testcases", () => {
    const productsData = [
      {
        name: "Bút chì 2B",
        description: "Bút chì nét nhỏ nhất",
        richDescription: "Bút giá rẻ sx vietnam",
        brand: "Dụng cụ học tập",
        price: "5000",
        countInStock: "10",
        rating: "3",
        numReviews: "200",
        isFeatured: "false",
      },
      {
        name: "Bút chì 3B",
        description: "Bút chì nét lớn để vẽ",
        richDescription: "Bút giá rẻ sx vietnam",
        brand: "Dụng cụ học tập",
        price: "6000",
        countInStock: "160",
        rating: "5",
        numReviews: "250",
        isFeatured: "true",
      },
      {
        name: "Bút chì màu",
        description: "Bút chì tô",
        richDescription: "Bút giá chất lượng cao",
        brand: "Dụng cụ vẽ",
        price: "16000",
        countInStock: "120",
        rating: "3",
        numReviews: "250",
        isFeatured: "true",
      },
      {
        name: "Bút chì 4B",
        description: "Bút chì nét nhỏ nhất",
        richDescription: "Bút giá rẻ sx vietnam",
        brand: "Dụng cụ học tập",
        price: "5000",
        countInStock: "10",
        rating: "3",
        numReviews: "200",
        isFeatured: "false",
      },
      {
        name: "Bút chì 5B",
        description: "Bút chì nét lớn để vẽ",
        richDescription: "Bút giá rẻ sx vietnam",
        brand: "Dụng cụ học tập",
        price: "6000",
        countInStock: "160",
        rating: "5",
        numReviews: "250",
        isFeatured: "true",
      },
      {
        name: "Bút chì màu",
        description: "Bút chì tô",
        richDescription: "Bút giá chất lượng cao",
        brand: "Dụng cụ vẽ",
        price: "16000",
        countInStock: "120",
        rating: "3",
        numReviews: "250",
        isFeatured: "true",
      },
    ];
    describe("Post/Product", () => {
      afterAll(async () => {
        await cleanup();
      });
      const imagePath = path.join(__dirname, "assets", "depzai.jpeg");

      productsData.forEach((productData) => {
        it(`should return new Product: ${productData.name}`, async () => {
          const createResponse = await request(app).post("/categories").send({
            name: "Quần áo",
            icon: "Clothes",
            color: "Blue",
          });

          expect(createResponse.statusCode).toEqual(200);
          const catId = createResponse.body._id;
          const res = await request(app)
            .post("/products")
            .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
            .field("name", productData.name)
            .field("description", productData.description)
            .field("richDescription", productData.richDescription)
            .field("brand", productData.brand)
            .field("price", productData.price)
            .field("category", `${catId}`)
            .field("countInStock", productData.countInStock)
            .field("rating", productData.rating)
            .field("numReviews", productData.numReviews)
            .field("isFeatured", productData.isFeatured);

          expect(res.statusCode).toEqual(200);
        });
      });
    });

    describe("Delete/Product", () => {
      afterAll(async () => {
        await cleanup();
      });
      const imagePath = path.join(__dirname, "assets", "depzai.jpeg");

      productsData.forEach((productData) => {
        it(`should return delete Product: ${productData.name}`, async () => {
          const createResponse = await request(app).post("/categories").send({
            name: "Quần áo",
            icon: "Clothes",
            color: "Blue",
          });

          expect(createResponse.statusCode).toEqual(200);
          const catId = createResponse.body._id;
          const res = await request(app)
            .post("/products")
            .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
            .field("name", productData.name)
            .field("description", productData.description)
            .field("richDescription", productData.richDescription)
            .field("brand", productData.brand)
            .field("price", productData.price)
            .field("category", `${catId}`)
            .field("countInStock", productData.countInStock)
            .field("rating", productData.rating)
            .field("numReviews", productData.numReviews)
            .field("isFeatured", productData.isFeatured);

          expect(res.statusCode).toEqual(200);
          const delProductResponse = await request(app).delete(
            `/products/${res.body._id}`
          );

          expect(delProductResponse.statusCode).toBe(200);
        });
      });
    });

    describe("Get/Product", () => {
      afterAll(async () => {
        await cleanup();
      });
      const imagePath = path.join(__dirname, "assets", "depzai.jpeg");

      productsData.forEach((productData) => {
        it(`should return get Product: ${productData.name}`, async () => {
          const createResponse = await request(app).post("/categories").send({
            name: "Quần áo",
            icon: "Clothes",
            color: "Blue",
          });

          expect(createResponse.statusCode).toEqual(200);
          const catId = createResponse.body._id;
          const res = await request(app)
            .post("/products")
            .attach("image", imagePath) // thay thế dduong dẫn tới file của bạn
            .field("name", productData.name)
            .field("description", productData.description)
            .field("richDescription", productData.richDescription)
            .field("brand", productData.brand)
            .field("price", productData.price)
            .field("category", `${catId}`)
            .field("countInStock", productData.countInStock)
            .field("rating", productData.rating)
            .field("numReviews", productData.numReviews)
            .field("isFeatured", productData.isFeatured);

          expect(res.statusCode).toEqual(200);
          const getProductResponse = await request(app).get(
            `/products/${res.body._id}`
          );

          expect(getProductResponse.statusCode).toBe(200);
        });
      });

      it(`should return get product all`, async (done) => {
        const getAllProductResponse = await request(app).get(`/products`);
        expect(getAllProductResponse.statusCode).toEqual(200);
        done();
      });

      it("should return get products/featured/5", async (done) => {
        const getProductResponse = await request(app).get(
          `/products/get/featured/5`
        );

        expect(getProductResponse.statusCode).toBe(200);
        done();
      });
      it("should return get count", async (done) => {
        const getProductResponse = await request(app).get(
          `/products/get/count`
        );

        expect(getProductResponse.statusCode).toBe(201);
        done();
      });

      it(`should return error: The product with the given ID not exists`, async (done) => {
        const getProductResponse = await request(app).get(`/products/-1`);
        expect(getProductResponse.statusCode).toEqual(500);
        expect(getProductResponse.body).toHaveProperty("success");
        expect(getProductResponse.body["success"]).toEqual(false);
        expect(getProductResponse.body).toHaveProperty("message");
        expect(getProductResponse.body["message"]).toEqual(
          "The product with the given ID not exists"
        );
        done();
      });
    });
  });
});
